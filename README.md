# GNU Guix, environnements reproductibles au delà des conteneurs ?

GNU Guix est un gestionnaire de environnements transactionnel et déclaratif
qui implémente une discipline de gestion de paquet fonctionnelle. Qu'est-ce
que cela signifie ? En quoi cela conduit-il à des environnements
computationnels reproductibles ? Reproductible d'un machine à l'autre ? Dans
le temps ? Et les conteneurs dans tout ca ? Nous tenterons de mettre ces
questions en perspective avec Guix.

GNU Guix fête cette année ses 10 ans. De nombreuses fonctionnalités ont été
ajoutées au fil des années et aujourd'hui Guix est un gestionnaire
d'environnements logiciels : il peut utiliser les paquets qu’il connaît pour
créer des environnements sous différentes formes, temporaires ou « permanentes
». Un environnement (collection de paquets) peut être déclaré par un manifeste
pour générer un « profil », chaque profil ayant son propre historique
permettant de revenir en arrière ou en avant. Un environnement temporaire peut
aussi être généré à la volée, et optionnellement isolé (conteneur Linux), ou
n'autorisant lecture, écriture ou réseau que spécifiquement. Aussi, ces
environnements peuvent être empaquetés dans des images Docker, Singularity ou
simplement des archives repositionnables. Pour finir, Guix permet, sur le même
principe, la génération de « machines virtuelles » à partir d'une déclaration
pouvant contenir un système d'exploitation. Une fois installé, Guix ne
nécessite pas de droits spécifiques pour manipuler ses différents
environnements.

https://capitoledulibre.org/
