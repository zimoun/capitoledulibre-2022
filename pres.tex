\documentclass[aspectratio=169,ignorenonframetext,presentation]{beamer}

%
% Header
%

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{fancyvrb}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, positioning, arrows.meta}

\newcommand{\tikzmark}[2]{%
  \tikz[remember picture,baseline=(#1.base)]%
  {\node[inner sep=0pt] (#1) {#2 \quad};}}

\newcommand{\tikzbrace}[3]{%
  \begin{tikzpicture}[remember picture,overlay]
    \draw[decorate,decoration={brace}] (#1.north east) -- node[right]
    { % XXXX: hfill
      #3}
    (#1.north east |- #2.south east);
  \end{tikzpicture}}


\usetheme{Madrid}
\useoutertheme[subsection=true]{smoothbars}

\makeatletter
\usecolortheme{whale}
\usecolortheme{orchid}
\setbeamerfont{block title}{size={}}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{itemize items}[triangle]
\makeatletter
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
      \usebeamerfont{author in head/foot}\quad S. Tournier
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
      \usebeamerfont{title in head/foot}\insertshorttitle
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

\title[Guix, gestionnaire d’environnements sous \emph{stéroïde}]%
{GNU Guix, environnements reproductibles au delà des conteneurs ?}
\author{Simon Tournier}
\date{Capitole du Libre, 20 novembre 2022}
%\institute{Inserm US53 - UAR CNRS 2030 \\ \texttt{simon.tournier@inserm.fr}}
\hypersetup{
  pdfauthor={Simon Tournier},
  pdftitle={GNU Guix, GNU Guix, environnements reproductibles au delà des conteneurs ??},
  pdfkeywords={GNU Guix, reproductibilité, déploiement logiciels, gestionnaire de paquets, machine virtuelle},
  pdfsubject={GNU Guix},
  pdfcreator={with love},
  pdflang={French}}


\newcommand{\violet}{\textcolor{violet}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\green}{\textcolor{green}}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\newcommand{\mauve}{\textcolor{mauve}}

\input{listings-scheme}

\newcommand{\someguile}{\textcolor{someguile}}
\newcommand{\somestring}{\textcolor{somestring}}
\newcommand{\somevariable}{\textcolor{somevariable}}

\newcommand{\thisslide}[1]{%
  \begin{figure}[!htb]
    \begin{center}
      \includeslide[width=\textwidth]{#1}
    \end{center}
  \end{figure}
}

\newcommand{\smiley}[1]{${\textsf{#1~}}^{\textrm{:-)}}$}

\newcommand{\hrefsf}[2]{\href{#1}{\textsf{#2}}}


%
% Document
%
\begin{document}

\begin{frame}[plain, fragile, noframenumbering]{}
  \vspace{5mm}
  \titlepage

  \vfill{}
  \vspace{-12mm}
  \begin{center}
    \includegraphics[width=0.2\paperwidth]{static/Guix-white}

    \vspace{-10mm}
    \href{https://guix.gnu.org}{\texttt{https://guix.gnu.org}}
    \\
    \href{https://foundation.guix.info}{\texttt{https://foundation.guix.info}}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section[Introduction]{\sout{Introduction}}


\begin{frame}<presentation>[label=scenarii, fragile]{Scenarii}
  \begin{itemize}
  \item Alice utilise \texttt{python@3.9} et \texttt{numpy@1.20.3}
    \vspace{-0.25cm}
    \begin{center}
      \begin{minipage}{0.75\linewidth}
        \begin{exampleblock}{}
\begin{verbatim}
$ sudo apt install python python-numpy
\end{verbatim}
        \end{exampleblock}
      \end{minipage}
    \end{center}
  \item Blake \textbf{collabore} avec Alice\ldots{}
    mais utilise \texttt{python3.8} et \texttt{numpy@1.16.5} pour un autre
    projet
    \vspace{-0.6cm}
    \begin{center}
      \begin{minipage}{0.75\linewidth}
        \begin{exampleblock}{}
\begin{verbatim}
$ apt-cache madison python-numpy
python-numpy | 1:1.16.5-2ubuntu7 | ...
\end{verbatim}
        \end{exampleblock}
      \end{minipage}
    \end{center}
  \item Charlie \textbf{mets à jour} son système et \textbf{tout est cassé}
    \vspace{-0.25cm}
    \begin{center}
      \begin{minipage}{0.75\linewidth}
        \begin{exampleblock}{}
\begin{verbatim}
$ sudo apt upgrade
The following packages have unmet dependencies:
E: Broken packages
\end{verbatim}
        \end{exampleblock}
      \end{minipage}
    \end{center}
  \item Bob utilise les \textbf{\alert{mêmes} versions} qu'Alice mais n'a
    \textbf{pas le \alert{même} résultat}
  \item Dan essaie de \textbf{rejouer plus tard} le scénario d'Alice
    mais rencontre l'\alert{enfer des dépendances}
  \end{itemize}
\end{frame}



\begin{frame}<presentation>[label=solutions, fragile]{Solution(s)}
  \begin{enumerate}
  \item gestionnaire de paquets : APT (Debian/Ubuntu), YUM (RedHat), etc.
  \item gestionnaire d'environnements : Conda, Pip, Modulefiles, etc.
  \item conteneur : Docker, Singularity
  \end{enumerate}

  \uncover<2->{
    \begin{alertblock}{}
      \begin{center}
        Guix = \#1 + \#2 + \#3
      \end{center}
    \end{alertblock}
  }

  \begin{description}
  \item[APT, Yum] Difficile de faire coexister plusieurs versions ou
    revenir en arrière ?
  \item[Pip/Conda] Quelle granularité sur la transparence ? (qui sait comment a
    été produit PyTorch dans \texttt{pip install torch} ?
    \href{http://hpc.guix.info/blog/2021/09/whats-in-a-package/}%
    {\scriptsize{(lien)}})
  \item[Modulefiles] Comment sont-ils maintenus ? (qui les utilise sur son
    \emph{laptop} ?)
  \item[Docker] Dockerfile basé sur APT, YUM etc.
    \begin{minipage}{1.0\linewidth}
      \begin{exampleblock}{}
\begin{verbatim}
RUN apt-get update && apt-get install
\end{verbatim}
      \end{exampleblock}
    \end{minipage}
  \end{description}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}<presentation>[label=steroide]{Guix est gestionnaire
    d’environnements sous \emph{stéroïde}}
  \begin{tabular}{lr}
    un \uline{\textbf{gestionnaire de paquets}}
    & (comme APT, Yum, etc.)
    \\
    \ \ transactionnel et déclaratif
    & (revenir en arrière, versions concurrentes)
    \\
    \ \ \ \ qui produit des \uline{\textbf{\emph{packs} distribuables}}
    & (conteneur Docker ou Singularity)
    \\
    \ \ \  \ \ \ qui génèrent des \textbf{\uline{\emph{machines virtuelles}} isolées}
    & (\emph{à la} Ansible ou Packer)
    \\
    \ \ \ \ \ \ \ \ sur lequel on construit une distribution Linux
    & (mieux que les \smiley{autres ?})
    \\
    \ \ \ \ \ \ \ \ \ \ \dots et aussi une bibliothèque Scheme\dots
    & (extensibilité, que voilà !)
  \end{tabular}

  \vfill

  \begin{alertblock}{}
    \begin{center}
      \textbf{20 minutes\dots}

      \dots c'est pour donner envie d'aller plus loin.
    \end{center}

    \begin{flushright}
      \small{(cette présentation est un apéritif quoi!)}
    \end{flushright}
  \end{alertblock}

  \begin{center}
    \textbf{\alert{Guix fonctionne sur n'importe quelle distribution Linux}}
  \end{center}

  \vfill

  \begin{flushright}
    \small{%
      (Facile à essayer\dots)
    }
  \end{flushright}
\end{frame}



\begin{frame}<presentation>[fragile]{Sur une \emph{distro externe}}
  \begin{alertblock}{}
    \begin{center}
      Guix s’installe sur \textbf{\uline{n’importe quelle distribution}} Linux récente.
    \end{center}
  \end{alertblock}
  Il faut les droits administrateur (\texttt{root}) pour l’installation (uniquement).

  \begin{exampleblock}{}
\begin{verbatim}
$ cd /tmp
$ wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
$ chmod +x guix-install.sh
$ sudo ./guix-install.sh
\end{verbatim}
  \end{exampleblock}

  \begin{center}
    (Quelques réglages supplémentaires, voir le manuel)
  \end{center}

  Pour commencer :
  \begin{center}
    \begin{minipage}{0.75\linewidth}
\begin{verbatim}
$ guix help
\end{verbatim}
    \end{minipage}
  \end{center}
\end{frame}


\begin{frame}<presentation>[plain, noframenumbering]{Ce que nous allons aborder}
  \begin{center}
    \begin{minipage}{0.7\textwidth}
      \begin{itemize}
      \item Gestionnaire de environnements transactionnel et déclaratif
      \item Reproductible d'une machine à l'autre ? Dans le temps ?
      \item Et les conteneurs dans tout ca ?
      \end{itemize}
    \end{minipage}
  \end{center}

  \setcounter{tocdepth}{2}
  \tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Gestionnaire de paquets}
\subsection{Commandes essentielles}


\begin{frame}<presentation>[label=cli-basic, fragile]{Guix, un gestionnaire de
    paquets comme les autres !}
  \vspace{-0.4cm}
  \begin{center}
    \footnotesize{(Python est un exemple, idem pour d’autres)}
    \normalsize
  \end{center}

  \vfill
  \begin{exampleblock}{}
\begin{verbatim}
guix search dynamically-typed programming language # 1.
guix show    python                                # 2.
guix install python                                # 3.
guix install python-ipython python-numpy           # 4.
guix remove  python-ipython                        # 5.
guix install python-matplotlib python-scipy        # 6.
\end{verbatim}
  \end{exampleblock}

  \vfill

  \begin{center}
    alias de \texttt{guix package}, p. ex. \verb+guix package --install+
  \end{center}

  \vfill
  \begin{alertblock}{Transactionnel}
\begin{verbatim}
guix package -r python-ipython -i python-matplotlib python-scipy # 5. & 6.
\end{verbatim}
  \end{alertblock}
\end{frame}



\begin{frame}<presentation>[label=pkg-man]{Guix, un gestionnaire de paquets comme les autres ?}
  \begin{alertblock}{}
    \begin{itemize}
    \item Interface \emph{ligne de commande} comme les autres gestionnaires de
      paquets
    \item Installation/suppression sans privilège particulier
    \item Transactionnel \hfill \small{(= pas d'état «~\emph{cassé}~»)}
    \item \emph{Substituts} binaires \hfill \small{(téléchargement d'éléments pré-construits)}
    \end{itemize}
  \end{alertblock}

  \begin{exampleblock}{}
    \begin{itemize}
    \item {Gestion déclarative}
    \item {Environnement isolé à la volée}
    \end{itemize}
  \end{exampleblock}

  \vfill
  Les \emph{profils} permettent la coexistence de plusieurs versions.

  \hfill \small{(\emph{profil} $\approx$ « environnement à la \texttt{virtualenv} »)}
\end{frame}

\subsection{Gestion déclarative}


\begin{frame}<presentation>[label=declarative, fragile]{Gestion déclarative}
  \vspace{-0.3cm}
  \begin{center}
    déclaratif = fichier de configuration
  \end{center}

  \begin{exampleblock}{%
      Un fichier \texttt{outils-projet.scm} peut contenir cette déclaration :%
    }
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python.scm}
  \end{exampleblock}

  \begin{center}
    \verb+guix package --manifest=outils-projet.scm+
  \end{center}
  équivalent à
  \begin{center}
    \verb+guix install python python-matplotlib python-numpy python-scipy+
  \end{center}
\end{frame}


\begin{frame}<presentation>[label=declarative2, fragile]{Gestion \emph{déclarative} : remarques}
  \begin{description}
  \item[Version ?] Nous le verrons dans la suite
    \medskip
  \item[Langage ?] \emph{Domain-Specific Language} (DSL) basé sur
    \href{https://fr.wikipedia.org/wiki/Scheme}%
    {Scheme}
    \href{https://fr.wikipedia.org/wiki/Lisp}%
    {(«~langage fonctionnel Lisp~»)}%
    \medskip
    \begin{itemize}
    \item \verb+(Oui (quand (= Lisp parenthèses) (baroque)))+
      \medskip
    \item Mais \uline{\textbf{continuum}} :
      \begin{enumerate}
      \item configuration (\texttt{manifest})
      \item définition des paquets (ou services)
      \item extension
      \item le c\oe ur est écrit aussi en Scheme
      \end{enumerate}
    \end{itemize}
  \end{description}
  \begin{alertblock}{}
    \begin{center}
      Guix est \textbf{adaptable} à ses besoins
    \end{center}
  \end{alertblock}

  \vfill

  \footnotesize{%
    \href{https://fr.wikipedia.org/wiki/Programmation_déclarative}{Déclaratif}
    vs
    \href{https://fr.wikipedia.org/wiki/Programmation_impérative}{Impératif}
  } \hfill \scriptsize{%
    (%
    et non pas
    % \hfill
    Donnée inerte vs Programme%
    )
  }

  \footnotesize{%
    Programmation déclarative = programmation fonctionnelle ou descriptive (\LaTeX) ou logique (Prolog)
  }
\end{frame}


\begin{frame}<presentation>[label=trans-declarative, fragile]{Gestion
    déclarative : exemple de transformation
    \hfill
    \small{%
      (\href{https://fr.wikipedia.org/wiki/Machine_de_Rube_Goldberg}%
      {machine de \smiley{Goldberg} \tiny{(lien)}})}} % \ddot\smile
  \vspace{-1.1cm}

  \hspace{-0.9cm}
  \begin{tabular}[t]{cc}
    \begin{minipage}[t]{0.625\linewidth}
      \begin{exampleblock}{}
        \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python-bis.scm}
      \end{exampleblock}
    \end{minipage}
    &
      \begin{minipage}[t]{0.375\linewidth}
        \uncover<2->{
          \vspace{0.9cm}
          \begin{exampleblock}{}
            \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python.scm}
          \end{exampleblock}
        }
      \end{minipage}
  \end{tabular}

  \vfill
  Guix DSL, \somevariable{\emph{variables}}, \someguile{Scheme} et \somestring{chaîne de caractères}.
\end{frame}

\subsection{Environnement isolé à la volée}


\begin{frame}<presentation>[label=shell,fragile]{Étendre temporairement un
    \emph{profil}
  \hfill \small(le coin du développeur)}
  \begin{exampleblock}{}
\begin{verbatim}
guix shell -m outils-projet.scm
guix shell -m outils-projet.scm python-ipython -- ipython3
\end{verbatim}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{tabular}{lcl}
      \red{$\blacktriangleright$} \verb+--pure+ &:& réinitialise des variables
                                                    d’environnement existantes
      \\
      \red{$\blacktriangleright$} \verb+--container+ &:& lance un conteneur isolé
      \\
      \uncover<2->{
      \red{$\blacktriangleright$} \texttt{-{}-development} &:& inclus les dépendances du paquet
      }
    \end{tabular}
  \end{alertblock}

  \begin{exampleblock}{}
\begin{verbatim}
guix shell -m outils-projet python-ipython             # 1.
guix shell -m outils-projet python-ipython --pure      # 2.
guix shell -m outils-projet python-ipython --container # 3.
\end{verbatim}
  \end{exampleblock}

  \uncover<2->{
  \begin{center}
    Bonus : \texttt{guix shell emacs git git:send-email -{}-development guix}
  \end{center}
  }
\end{frame}


\begin{frame}<presentation>[plain,label=prod,noframenumbering]{}
  \begin{center}
    \textbf{Coupure pub : Guix et la recherche scientifique}

    \small{(Guix en production)}
  \end{center}

  \vspace{-0.2cm}
  \begin{exampleblock}{}
    \begin{tabular}{lcrl|c}
      \textbf{Grid’5000}           &          &  828-nodes  &(12,000+ cores, 31 clusters) &(France)     \\
      \textbf{GliCID (CCIPL)}      & Nantes   &  392-nodes  &(7500+ cores)                 &(France)    \\
      \textbf{PlaFrIM Inria}       & Bordeaux &  120-nodes  &(3000+ cores)                 &(France)    \\
      \textbf{GriCAD}              & Grenoble &  72-nodes   &(1000+ cores)                 &(France)    \\
      \textbf{Max Delbrück Center} & Berlin   &  250-nodes  &+ workstations                &(Allemagne) \\
      \textbf{UMC}                 & Utrecht  &  68-nodes   &(1000+ cores)                 &(Pays-Bas)  \\
      \textbf{UTHSC Pangenome}     &          &  11-nodes   &(264 cores)                   &(USA)       \\
      \\
      \quad (le vôtre ?)
    \end{tabular}
  \end{exampleblock}

  \vspace{-0.25cm}
  \begin{center}
      \includegraphics[width=0.25\paperwidth]{static/guixhpc-logo-transparent-white}

    \vspace{-0.25cm}

    \href{https://hpc.guix.info/}{\texttt{https://hpc.guix.info}}
  \end{center}

  \vspace{-1.2cm}
  \begin{flushright}
    \includegraphics[width=0.175\paperwidth]{static/cafe-guix}
  \end{flushright}
\end{frame}


\begin{frame}<presentation>[plain,fragile,noframenumbering]{}
  À ce stade (\texttt{guix shell -{}-container}),
  \begin{flushright}
    il semble naturel de vouloir \uline{partager ou construire des conteneurs isolés}.
  \end{flushright}

  \vfill{}
  \begin{center}
    \textbf{Comment créer des images ?}
  \end{center}
  \vfill{}
  \begin{flushright}
    (Exemple : Alice sans privilège particulier)
  \end{flushright}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Création d'images}
\subsection{Création d’un \emph{pack}}


\begin{frame}<presentation>[label=conteneur,fragile]{Comment capturer un
    environnement ?
    \hfill\small{(avec un conteneur ?)}}
  \begin{center}
    Conteneur = \smiley{smoothie}
  \end{center}

  \begin{itemize}
  \item Comment est construit le conteneur ? Dockerfile ?
  \item Comment sont construits les binaires inclus dans le conteneur ?
  \end{itemize}

  \begin{exampleblock}{}
\begin{verbatim}
FROM amd64/debian:stretch
RUN apt-get update && apt-get install git make curl gcc g++ ...
RUN curl -L -O https://... && ... && make -j 4 && ...
RUN git clone https://... && ... && make ... /usr/local/lib/libopenblas.a ...
\end{verbatim}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      Avec un Dockerfile au temps t, comment regénérer l'image au temps t’ ?
    \end{center}
  \end{alertblock}
\end{frame}


\begin{frame}<presentation>[label=pack, fragile]{Qu’est-ce qu’un \emph{pack} ?
  \hfill\small{(c’est un conteneur, non ?)}}
  \begin{center}
    \emph{pack} = collection de paquets dans un format d’archive
  \end{center}

  Quel est le but d’un \emph{pack} ?
  \begin{itemize}
  \item Alice  distribue «~tout~» à Blake,
  \item Blake n’a pas installé Guix mais aura l’exact même environnement.
  \end{itemize}

  \uncover<2->{
  \begin{exampleblock}{Qu’est-ce qu’un format d’archive ?}
    \begin{itemize}
    \item \texttt{tar} (\emph{tarballs})
    \item Docker
    \item Singularity
    \item paquet binaire Debian \texttt{.deb}
    \end{itemize}
  \end{exampleblock}
  }
\end{frame}


\begin{frame}<presentation>[label=tout, fragile]{Qu'est-ce que «~tout~» ?}
  \begin{center}
    Blake a besoin de la \emph{clôture transitive} (= toutes les dépendances)
  \end{center}
  \begin{exampleblock}{}
\begin{verbatim}
$ guix size python-numpy --sort=closure
store item             total    self
python-numpy-1.20.3    301.5    23.6   7.8%
...
python-3.9.9           155.3    63.7  21.1%
openblas-0.3.18        152.8    40.0  13.3%
...
total: 301.5 MiB
\end{verbatim}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      \texttt{guix pack} permet de créer cette archive contenant «~tout~»
    \end{center}
  \end{alertblock}
\end{frame}


\begin{frame}<presentation>[label=pack2, fragile]{Création d’un \emph{pack}
    pour le distribuer}

  \begin{itemize}
  \item Alice construit un \emph{pack} au format Docker
    \begin{exampleblock}{}
      \begin{center}
        \verb+guix pack --format=docker -m outils-projet.scm+
      \end{center}
    \end{exampleblock}
    puis distribue ce conteneur Docker (via un \emph{registry} ou autre).

  \item<2-> Blake n'utilise pas \small{(encore?)}\normalsize{} Guix
    \begin{exampleblock}{}
\begin{verbatim}
$ docker run -ti projet-alice python3
Python 3.9.9 (main, Jan  1 1970, 00:00:01)
[GCC 10.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
\end{verbatim}
    \end{exampleblock}
    et utilise l'exact même environnement computationnel qu'Alice.
  \end{itemize}

  \uncover<3->{
    \begin{alertblock}{}
      \begin{center}
        \href{https://hpc.guix.info/blog/2021/10/when-docker-images-become-fixed-point/}%
        {\small{(Reconstruire l’exact même \emph{pack} Docker avec Guix sur 2 machines à
            différents moments \tiny{(lien)})}}
      \end{center}
    \end{alertblock}
  }
\end{frame}


\begin{frame}<presentation>[label=pack3, fragile]{Au final, \texttt{guix pack}, c’est\dots}

  \begin{center}
    \emph{Agnostique} sur le format du «~conteneur~»
  \end{center}

  \begin{tabular}{cc}
    \begin{minipage}{0.4\linewidth}
      \begin{exampleblock}{}
        \begin{itemize}
        \item \texttt{tar} (\emph{tarballs})
        \item Docker
        \item Singularity
        \item paquet binaire Debian \texttt{.deb}
        \end{itemize}
      \end{exampleblock}
    \end{minipage}
    &
      \begin{minipage}{0.5\linewidth}
        \begin{alertblock}{}
          \begin{itemize}
          \item archives repositionnables
          \item sans \texttt{Dockerfile}
          \item via \texttt{squashfs}
          \item sans \texttt{debian/rule} \hfill \small{(expérimental)}\normalsize
          \end{itemize}
        \end{alertblock}
      \end{minipage}
  \end{tabular}

  \bigskip

  \begin{center}
    \textbf{Adaptable aux cas d’usage}
  \end{center}
\end{frame}

\subsection{Création d'une machine virtuelle}


\begin{frame}<presentation>[label=image, fragile]{Création d’une image avec
    \texttt{guix system}
    \hfill \small{(}un monde de services)}

  \begin{alertblock}{}
    \begin{center}
      \texttt{guix system} permet une
      \uline{\textbf{configuration déclarative} d'un \emph{système}}
    \end{center}
  \end{alertblock}

  \begin{itemize}
  \item \texttt{guix system search} pour trouver les services disponibles
  \item \texttt{guix system image} pour construire une image de type :
    \begin{itemize}
    \item qcow2
    \item docker
    \item iso9660, uncompressed-iso9660, efi-raw, raw-with-offset
    \item rock64-raw, pinebook-pro-raw, pine64-raw, novena-raw
    \item hurd-raw, hurd-qcow2
    \end{itemize}
  \item
    \begin{minipage}{1.0\linewidth}
      \begin{exampleblock}{}
        \begin{center}
          \texttt{guix system vm} pour construire une machine virtuelle (VM)
        \end{center}
      \end{exampleblock}
    \end{minipage}
  \end{itemize}
\end{frame}


\begin{frame}<presentation>[fragile]{Configuration déclarative
    d'une machine virtuelle}
  \vspace{-0.3cm}
  \begin{exampleblock}{}
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/config-vm-1.scm}
  \end{exampleblock}
\end{frame}


\begin{frame}<presentation>[plain,noframenumbering]{}
  \begin{center}
    \textbf{Coupure pub : Guix et sa communauté}
  \end{center}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Créé en 2012 par Ludovic Courtès \hfill (Guix = Guile avec Nix)

      \scriptsize{
        \hfill Nix (NixOS) initie la gestion de paquets dans un paradigme fonctionnel.
      }\normalsize
    \item 104000+ commits par $\approx$800 personnes
    \item v1.0 en 2019
    \item \href{https://guix.gnu.org/blog/}{https://guix.gnu.org/blog/}
    \item 2 conférences en ligne, hackathon, Guix Days avant le FOSDEM
    \item \href{https://10years.guix.gnu.org/}{https://10years.guix.gnu.org}

      \hfill https://10years.guix.gnu.org/video/ten-years-of-failures/
    \end{itemize}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      \textbf{accueillante} et \textbf{bienveillante}
    \end{center}
  \end{alertblock}

  %\vspace{-0.25cm}
  \begin{center}
    \href{https://foundation.guix.info/}{\texttt{https://foundation.guix.info}}
  \end{center}

  %\vspace{-1.2cm}
  \begin{flushright}
    \includegraphics[width=0.175\paperwidth]{static/10-years-banner.png}
  \end{flushright}
\end{frame}


\begin{frame}<presentation>[plain,fragile,noframenumbering]{}
  Très bien tout cela, mais en quoi est-ce \uline{reproductible} ?

  \vfill{}
  \begin{center}
    \textbf{Parlons de versions !}
  \end{center}
  \vfill{}
  \begin{flushright}
    (Exemple :  d'une machine à l'autre)
  \end{flushright}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Reproductibilité}


\begin{frame}<presentation>[label=version, fragile]{Quelle est ma version de Guix ?}
  % \vspace{-0.25cm}
  \begin{exampleblock}{}
\begin{verbatim}
$ guix describe
Generation 76	Apr 25 2022 12:44:37	(current)
  guix eb34ff1
    repository URL: https://git.savannah.gnu.org/git/guix.git
    branch: master
    commit: eb34ff16cc9038880e87e1a58a93331fca37ad92

$ guix --version
guix (GNU Guix) eb34ff16cc9038880e87e1a58a93331fca37ad92
\end{verbatim}
  \end{exampleblock}

  % \vspace{-0.175cm}
  \begin{alertblock}{}
    \begin{center}
      Un état fixe toute la collection des paquets et de Guix lui-même
    \end{center}
  \end{alertblock}

  \footnotesize{
  (Un état peut contenir plusieurs canaux (\emph{channel} = dépôt Git),\\
  \hfill avec des URL, branches ou commits divers et variés)
  }
\end{frame}


\begin{frame}<presentation>[label=dag, fragile]{État = Graphe Acyclique Dirigé (\emph{DAG})}
  \vspace{-0.5cm}
  \begin{center}
    \texttt{guix graph -{}-max-depth=6 python | dot -Tpng > graph-python.png}
  \end{center}

  \vfill{}
  \includegraphics[width=\textwidth]{static/graph-python.png}
  \vfill{}

  Graphe complet : Python = 137 n\oe uds, Numpy = 189, Matplotlib = 915, Scipy
  = 1439 n\oe uds
% graph python-scipy -t bag | grep label | cut -f2 -d'[' | sort | uniq | wc -l
\end{frame}


\begin{frame}<presentation>[label=def-pkg, fragile]{Définition d’un paquet
    \hfill\small{(n\oe ud du graphe)}}
  \vspace{-0.3cm}
  \begin{exampleblock}{}
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/mock-define-python.scm}
  \end{exampleblock}

  \vspace{-0.2cm}
  \uncover<2->{
  \begin{alertblock}{}
    \begin{itemize}
    \item Chaque \texttt{inputs} est une définition similaire
      \hfill (récursion $\rightarrow$ graphe)
    \item Il n’y a pas de cycle
      \hfill (\texttt{bzip2} ou ses \texttt{inputs} ne peuvent pas utiliser \texttt{python})
    \end{itemize}
  \end{alertblock}
  }
  \vspace{-0.25cm}
  \uncover<3->{
  \begin{center}
    \small{%
    (Quel commencement du graphe ?  Problème du \emph{bootstrap} dont nous ne
    parlerons pas ici)
    }\normalsize
  \end{center}
  }
\end{frame}


\begin{frame}<presentation>[label=pull, fragile]{Concrètement, en pratique ?}

  \begin{alertblock}{}
    \begin{center}
      une version = un graphe
    \end{center}
  \end{alertblock}

  \begin{center}
    Mise à jour de Guix

    \texttt{guix pull}
  \end{center}

  \begin{itemize}
  \item<2-> Il est possible de \uline{spécifier un \emph{état}} :
    \begin{exampleblock}{}
      \begin{center}
        \begin{tabular}{rl}
          \blue{\texttt{alice@laptop\$}} & \texttt{guix describe -{}-format=channels >
                                           \red{etat-alice.scm}}
          \\
          \violet{\texttt{blake@desktop\$}} & \texttt{guix pull -{}-channels=\red{etat-alice.scm}}
        \end{tabular}
      \end{center}
    \end{exampleblock}
  \item<3-> Il est possible de se placer \uline{\textbf{temporairement} dans un état spécifique}\\
    \hfill pour exécuter une commande (comme créer un profil)

    \begin{exampleblock}{}
      \begin{center}
\begin{verbatim}
$ guix time-machine --channels=etat-alice.scm \
       -- install python -p python-alice
\end{verbatim}
      \end{center}
    \end{exampleblock}
  \end{itemize}
\end{frame}


\begin{frame}<presentation>[label=time-machine, fragile]{Reproductibilité
    \hfill
    \small
    \texttt{outils-projet.scm} + \texttt{etat-alice.scm} + \texttt{guix time-machine}}

  \vspace{-1cm}
  \begin{center}
    \begin{tikzpicture}
      % draw horizontal line
      \draw[thick, -Triangle] (0,0) -- (12,0) node[font=\scriptsize,below left=3pt and -8pt]{time};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (0,-.3) {2018};

      % draw vertical lines
      \foreach \x in {1,...,11}
      \draw (\x cm,3pt) -- (\x cm,-3pt);

      \foreach \x/\descr in {3/Blake,6/Alice,8/Carole}
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (\x,-.3) {$\descr$};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Blake) at (3,+.3)  {d7e57e};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Alice) at (6,+.3)  {eb34ff1};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Dan) at (8,+.3)  {3682bd};

      \draw[->] (Blake.north) to [out=50,in=150] (Alice.west);
      \draw[->] (Dan.north) to [out=100,in=50] (Alice.east);

      \draw[lightgray!0!red, line width=4pt] (2,-.6) -- +(8,0);
      \foreach \x/\percol in {1/75,9/75}
      \draw[lightgray!\percol!red, line width=4pt] (\x,-.6) -- +(1,0);

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (11,-.3)  {$Dan$};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Dan) at (11,+.3)  {c99c3d};
      \draw[dashed,->] (Dan.north) to [out=40,in=90] (Alice.north);

    \end{tikzpicture}
  \end{center}

  \begin{exampleblock}{}
    Pour être \uline{reproductible dans le temps}, il faut :
    \begin{itemize}
    \item Une préservation de \textbf{tous} les codes source~%
      \small{%
        (\href{https://ngyro.com/pog-reports/latest/}%
        {$\approx 75\%$ archivés \tiny{(lien)}\small} dans~%
        \href{https://www.softwareheritage.org/}%
        {Software Heritage \tiny{(lien)}\small})%
      }\normalsize
    \item Une \emph{backward} compatibilité du noyau Linux
    \item Une compatibilité du \emph{hardware} (p. ex. CPU, disque dur \tiny{(NVMe)}\normalsize, etc.)
    \end{itemize}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      Quelle est la taille de la fenêtre temporelle avec les 3 conditions
      satisfaites ?
    \end{center}
  \end{alertblock}
  \begin{center}
    \scriptsize{(À ma connaissance, le projet Guix réalise une expérimentation
      grandeur nature et quasi-unique depuis sa v1.0 en 2019)}
  \end{center}
\end{frame}



\begin{frame}<presentation>[plain,fragile,noframenumbering]{}
  20 minutes sont déjà passées, non ?
  \vfill{}
  \begin{center}
    \textbf{Il est temps de s'arrêter}
  \end{center}
  \vfill{}
  \begin{flushright}
    Résumons !
  \end{flushright}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Au final}
\subsection{En conclusion\dots}


\begin{frame}<presentation>[label=continuum2, fragile]{\dots Guix est un \emph{continuum}%
    \hfill \small{(sur n'importe quelle distribution Linux)}}

  \vspace{-0.5cm}

  \begin{center}
    \begin{tabular}{llr}
      un \uline{\textbf{gestionnaire de paquets}} déclaratif
      & \texttt{guix package}
      & (\texttt{-m} \emph{manifest})
      \\
      \ \ temporairement étendu à la volée
      & \texttt{guix shell}
      & (\texttt{-{}-container})
      \\
      \ \ \ \ maîtrisant exactement l'\emph{état}
      & \texttt{guix time-machine}
      & (\texttt{-C} \emph{channels})
      \\
      \ \ \ \ \ \ qui produit des \uline{\textbf{\emph{packs} distribuables}}
      & \texttt{guix pack}
      & (\texttt{-f docker})
      \\
      \ \ \ \ \ \ \ \ qui génèrent des \textbf{\uline{\emph{machines virtuelles}} isolées}
      & \texttt{guix sytem vm}
      \\
      \ \ \ \ \ \ \ \ \ \ \ \ \ et aussi une bibliothèque Scheme
      & \texttt{guix repl}
      & (\emph{extensions})
      \\
      $\Big($
      la distribution Linux elle-même
      & \texttt{config.scm}
      & (Guix System)
        $\Big)$
    \end{tabular}

    \uncover<2->{
    \begin{alertblock}{}
      \begin{center}
        \textbf{%
          Guix permet un contrôle fin du graphe de configuration sous-jacent
        }
      \end{center}
    \end{alertblock}
  \end{center}
  }

  \uncover<3->{
  \begin{exampleblock}{}
    \begin{center}
      \texttt{guix time-machine -C \blue{etat.scm} -{}-}
      \emph{commande options}
      \texttt{\blue{une-config.scm}}
    \end{center}
  \end{exampleblock}
  }

  \uncover<4->{
  \begin{alertblock}{}
    \begin{center}
      \texttt{\blue{une-config.scm}} est \textbf{reproductible}
      d'une machine à l'autre et dans le temps
    \end{center}
  \end{alertblock}
  }
\end{frame}


\begin{frame}<presentation>[label=scenarii2,fragile]{Scenarii}
  \vspace{-0.2cm}
  \begin{itemize}
  \item Alice utilise \texttt{python@3.9} et \texttt{numpy@1.20.3}
    \vspace{-0.1cm}
    \begin{exampleblock}{}
\begin{verbatim}
$ guix install python python-numpy
\end{verbatim}
    \end{exampleblock}
  \item Blake \textbf{collabore} avec Alice\ldots{}
    mais utilise d'autres outils pour un autre projet
    \vspace{-0.1cm}
    \begin{exampleblock}{}
\begin{verbatim}
$ guix time-machine -C version-alice.scm \
       -- install -m outils-alice.scm -p projet/alice
\end{verbatim}
        \end{exampleblock}
  \item Charlie \textbf{mets à jour} son système et \textbf{tout est cassé}
    \vspace{-0.1cm}
    \begin{exampleblock}{}
\begin{verbatim}
$ guix pull --roll-back
\end{verbatim}
    \end{exampleblock}
  \item Bob utilise les \textbf{{mêmes} versions} qu'Alice mais n'a
    \textbf{pas le {même} résultat}
    \vspace{-0.1cm}
    \begin{exampleblock}{}
\begin{verbatim}
error: You found a bug
\end{verbatim}
    \end{exampleblock}
  \item Dan essaie de \textbf{rejouer plus tard} le scénario d'Alice\dots
    \vspace{-0.25cm}
    \begin{flushright}
      \scriptsize{(voir ligne commande Blake)}\normalsize
      \hfill{}
      \dots ça dépend de la date du scénario ;-)
    \end{flushright}
  \end{itemize}
\end{frame}


\begin{frame}<presentation>[label=limit,fragile]{Convaincu ?}
  \begin{itemize}
  \item Fonctionne uniquement avec Linux.
  \item Environnement isolé implique une forte transparence,
    \begin{flushright}
      c.-à-d., difficile avec des parties propriétaires (pilotes).
    \end{flushright}

  \item Certaines commandes peuvent apparaître lentes (\texttt{pull}, \texttt{search}, etc.),
    \begin{flushright}
      ou retourner des erreurs obscures.
    \end{flushright}
  \item Les premiers pas requièrent un peu de patience,
    \begin{flushright}
      et d’accepter que ce
      n’est \uline{pas \emph{comme d’habitude}}.
    \end{flushright}
  \end{itemize}

  \vfill{}
  \begin{alertblock}{}
    \begin{center}
      \textbf{La communauté est très accueillante et toujours disponible pour aider}
    \end{center}
  \end{alertblock}
\end{frame}



\begin{frame}[plain, noframenumbering]{}
  \begin{center}
    \vfill{}
    \Large{%
      \textbf{%
        Des questions ?%
      }}\normalsize

    \vfill{}
    \texttt{help-guix@gnu.org}
    \vfill{}
    \textbf{IRC} libera.chat : \texttt{\#guix}

    \vfill{}
    \vspace{1.5cm}
    Merci à Julien Lepiller pour la discussion hier sur cette présentation :-)\\
    \includegraphics[width=0.1\paperwidth]{static/Guix-white}\\
  \end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix


\begin{frame}<presentation>[label=transformation, fragile,noframenumbering]{Transformations de paquet : survol}
    \begin{exampleblock}{}
    \begin{center}
      Comment utiliser GCC@7
      \scriptsize{(au lieu de GCC@10)}
      \normalsize{}
      pour compiler le paquet \texttt{python} \scriptsize{(CPython)} ?
    \end{center}
  \end{exampleblock}

  \begin{center}
    Un paquet = recette pour configurer, construire, installer un logiciel

    \medskip

    (\texttt{./configure \&\& make \&\& make install})
  \end{center}
  La recette définit :
  \begin{itemize}
  \item un \blue{code source} \hfill (et potentiellement des modifications \emph{ad-hoc} \verb+patch+)
  \item des \magenta{outils de construction} \hfill (compilateurs, moteur de
    production etc., p. ex. \verb+gcc+, \verb+cmake+)
  \item des \violet{dépendances} \hfill (autres paquets pour configurer ou construire)
  \end{itemize}

  \begin{alertblock}{}
    \begin{center}
      Une transformation permet de les réécrire
    \end{center}
  \end{alertblock}
\end{frame}


\begin{frame}<presentation>[label=cli-trans, fragile,noframenumbering]{Transformations : ligne de commande}
  \begin{exampleblock}{}
    \begin{center}
      \verb+guix package --help-transformations+
    \end{center}
  \end{exampleblock}
\begin{Verbatim}[commandchars=\\\{\}]
\blue{--with-source      use SOURCE when building the corresponding package}
\blue{--with-branch      build PACKAGE from the latest commit of BRANCH}
\blue{--with-commit      build PACKAGE from COMMIT}
\blue{--with-git--url    build PACKAGE from the repository at URL}
\blue{--with-patch       add FILE to the list of patches of PACKAGE}
\blue{--with-latest      use the latest upstream release of PACKAGE}
\magenta{--with-c-toolchain build PACKAGE and its dependents with TOOLCHAIN}
\magenta{--with-debug-info  build PACKAGE and preserve its debug info}
\magenta{--without-tests    build PACKAGE without running its tests}
\violet{--with-input       replace dependency PACKAGE by REPLACEMENT}
\violet{--with-graft       graft REPLACEMENT on packages that refer to PACKAGE}
\end{Verbatim}
\end{frame}


\begin{frame}<presentation>[label=trans-manif, fragile,noframenumbering]{Transformations via fichier manifeste}
  \vspace{-0.3cm}
  \begin{exampleblock}{}
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python-with-gcc7.scm}
  \end{exampleblock}
\end{frame}
\end{document}
